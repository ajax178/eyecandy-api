from sqlalchemy.orm import Session
from datetime import datetime
import models
import schemas

def get_image(db: Session, image_id: int):
    return db.query(models.Image).filter(models.Image.id == image_id).first()

def get_images(db: Session, skip: int = 0, limit: int = 10):
    return db.query(models.Image).offset(skip).limit(limit).all()

def create_image(db: Session, image: schemas.ImageCreate):
    db_image = models.Image(thumbnail_base64=image.thumbnail_base64, full_size_url=image.full_size_url, upload_date=datetime.now())
    db.add(db_image)
    db.commit()
    db.refresh(db_image)
    return db_image
    
def delete_image(db: Session, image_id: int):
    db_image_to_delete = db.query(models.Image).filter_by(id = image_id).one_or_none()
    db.delete(db_image_to_delete)
    db.commit()
    return db_image_to_delete
