from fastapi import FastAPI, Depends, HTTPException, Request, Header, File, UploadFile, Response
from fastapi.security import OAuth2PasswordBearer
from fastapi.middleware.cors import CORSMiddleware
from typing import Optional, List
import httpx
from httpx import AsyncClient
from starlette.config import Config

from starlette.responses import FileResponse
import shutil
import os
from datetime import datetime
from PIL import Image, ExifTags
import base64
from io import BytesIO

import models
import crud
import schemas
from database import SessionLocal, engine
from sqlalchemy.orm import Session

config = Config('.env')

app = FastAPI()

models.Base.metadata.create_all(bind=engine)
def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()

origins = [
    "http://localhost:8081",
    "http://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

oauth2_scheme = OAuth2PasswordBearer(tokenUrl='token')

#def verify_token(access_token: Optional[str]):
def verify_token(authorization: str = Header(None)):
    is_authenticated = False
    try:
        headers = {
            'authorization': authorization,
        }
        response = httpx.get('http://localhost:8080/auth/realms/GalleryApiAuth/protocol/openid-connect/userinfo', headers=headers)
    except:
        return is_authenticated
    if response.status_code == 200:
            is_authenticated = True
            return is_authenticated
    else:
        return is_authenticated

@app.post('/upload')
#async def image_upload(image: UploadFile = File(...), authorization: Optional[str] = Header(default=None), status_code=201):
def image_upload(images: List[UploadFile] = File(...), db: Session = Depends(get_db)):
    #auth_status = verify_token(authorization)
    auth_status: Bool = Depends(verify_token)
    if auth_status:
        for image in images:
            now = str(datetime.now())
            now = now.replace(":","_")
            now = now.replace(" ","_")
            file_location: String = f"images/{now}"
            #file_location: String = f"images/{image.filename}"
            with open(file_location, "wb+") as file_object:
                shutil.copyfileobj(image.file, file_object) #write to disk
                imagefile = Image.open(file_object)
                
                #check exif data for rotation
                try:
                    for orientation in ExifTags.TAGS.keys():
                        if ExifTags.TAGS[orientation]=='Orientation':
                            break

                    exif=imagefile._getexif()
                    if exif[orientation] == 3:
                        imagefile=imagefile.rotate(180, expand=True)
                    elif exif[orientation] == 6:
                        imagefile=imagefile.rotate(270, expand=True)
                    elif exif[orientation] == 8:
                        imagefile=imagefile.rotate(90, expand=True)
                except:
                    pass
                
                #process thumbnail 
                imagefile.convert('RGB')
                size = 500, 500
                imagefile.thumbnail(size)
                with BytesIO() as thumb_io:
                    imagefile.save(thumb_io, 'JPEG', quality=85)
                    thumb_io.seek(0)
                    thumbnail_base64 = base64.b64encode(thumb_io.read())
                    
                #add to database
                    class image_info:
                        def __init__(self, thumbnail_base64: str, full_size_url: str):
                            self.thumbnail_base64 = thumbnail_base64
                            self.full_size_url = full_size_url
                            
                    o=image_info(thumbnail_base64.decode('utf-8'), now)
                    crud.create_image(db=db, image=o)
        return Response(status_code=201, content="Upload success")
    else:
        raise HTTPException(status_code=401, detail="Authentication Required")
    
@app.delete('/images/{image_id}')
async def delete_image(image_id: int, db: Session = Depends(get_db)):
    auth_status: Bool = Depends(verify_token)
    if auth_status:
        db_image = crud.delete_image(db, image_id=image_id)
        os.remove(f"images/{db_image.full_size_url}")
        if db_image is None:
            raise HTTPException(status_code=400, detail="Image not found")
        return db_image

@app.get('/images/{image_id}')
async def get_image(image_id: int, db: Session = Depends(get_db)):
    db_image = crud.get_image(db, image_id=image_id)
    if db_image is None:
        raise HTTPException(status_code=404, detail="image not found")
    return db_image

@app.get('/images/full_size/{full_size_url}')
async def get_full_image(full_size_url: str):
    try:
        return FileResponse(f"images/{full_size_url}", media_type='image/jpeg', filename=full_size_url)
    except:
        raise HTTPException(status_code=404, detail="image not found")

@app.get('/images/', response_model=List[schemas.Image])
async def get_list_images(skip: int = 0, limit: int = 10,  db: Session = Depends(get_db)):
    images = crud.get_images(db, skip=skip, limit=limit)
    return images

@app.get('/')
def read_root():
    return {"Hello": "World"}

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, port = 8000)