from sqlalchemy import Boolean, Column, Integer, String, DateTime
from database import Base

class Image(Base):
    __tablename__ = "images"
    
    id = Column(Integer, primary_key=True, index=True)
    full_size_url = Column(String, unique=True, index=True)
    thumbnail_base64 = Column(String)
    upload_date = Column(DateTime)
    
