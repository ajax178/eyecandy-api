from typing import List
from datetime import datetime
from pydantic import BaseModel

class ImageBase(BaseModel):
    thumbnail_base64: str
    full_size_url: str
    upload_date: datetime

class ImageCreate(ImageBase):
    pass
    
class Image(ImageBase):
    id: int    
    class Config:
        orm_mode = True
